# -*- coding: utf-8 -*-
import pytest
from argparse import ArgumentParser
from dataclasses import dataclass

from snregales.classes.abstract.parser import AbstractParser


@pytest.fixture
def parser():
    AbstractParser.__abstractmethods__ = set()

    @dataclass
    class Parser(AbstractParser):
        _AbstractParser__parser = ArgumentParser()

    return Parser()
