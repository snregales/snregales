# -*- coding: utf-8 -*-
import pytest
from dataclasses import dataclass
from argparse import _ArgumentGroup, ArgumentParser, Namespace

from snregales.classes.abstract.parser import AbstractParser
from . import parser


def test_abstractparser() -> None:
    with pytest.raises(TypeError):
        AbstractParser()


def test_abstractparser_components() -> None:
    abstract_parser = AbstractParser.__dict__.keys()
    for method in (
        "_optional_arguments",
        "_required_arguments",
        "args",
        "parser",
        "_AbstractParser__required_argument_group",
    ):
        assert method in abstract_parser


def test_abstractparser_abstractmethods() -> None:
    print(AbstractParser.__dict__.keys())
    assert AbstractParser.__abstractmethods__ == frozenset({"_required_arguments"})


def test_abstractparser_properties(parser) -> None:  # noqa: F811
    assert isinstance(parser.parser, ArgumentParser)
    assert isinstance(parser._AbstractParser__required_argument_group, _ArgumentGroup)


def test_abtsractparser_abtractmethod_none(parser) -> None:  # noqa: F811
    assert parser._optional_arguments(parser.parser) is None
    assert (
        parser._required_arguments(parser._AbstractParser__required_argument_group)
        is None
    )
